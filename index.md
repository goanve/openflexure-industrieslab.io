---
layout: home
---

# OpenFlexure Industries

## Printing open source laboratory-grade robotic microscopes.
![](/assets/MicroscopeAdjust.jpg)

## OpenFlexure Industries sells open source hardware developed by the [OpenFlexure project](https://openflexure.org).